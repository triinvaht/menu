<?php
require 'vendor/autoload.php';

$app = new Slim\App();
$container = $app->getContainer();
$container['environment'] = function () {
    $scriptName = $_SERVER['SCRIPT_NAME'];
    //$_SERVER['SCRIPT_NAME'] = dirname(dirname($scriptName)) . '/' . basename($scriptName);
    //$_SERVER['SCRIPT_NAME'] = '/item';
    return new Slim\Http\Environment($_SERVER);
};

/**
 * POST addItem
 * Summary: Add a new item
 * Notes:
 * Output-Formats: [application/xml, application/json]
 */

$app->POST('/item', function ($request, $response, $args) {
    require_once('db.php');
    require_once('checkLogin.php');

    if (checkToken()) {
        $query = "INSERT INTO menu (item, category, price) VALUES (?,?,?)";

        /* @var $conn */
        $stmt = $conn->prepare($query);

        $stmt->bind_param("ssi", $item, $category, $price);

        $item = $request->getParsedBody()['item'];

        $category = $request->getParsedBody()['category'];

        $price = $request->getParsedBody()['price'];

        $stmt->execute();

        $body = $request->getParsedBody();
        $response->write();
        return $response;
    } else {
        return $response->withStatus(401);
    }
});

/**
 * DELETE deleteItem
 * Summary: Deletes an item
 * Notes:
 * Output-Formats: [application/xml, application/json]
 */
$app->DELETE('/item/{item_id}', function ($request, $response, $args) {
    $headers = $request->getHeaders();
    require_once('db.php');
    require_once('checkLogin.php');

    if (checkToken()) {
        $get_item_id = $request->getAttribute('item_id');

        $query = "DELETE from menu WHERE item_id = $get_item_id";

        $result = $conn->query($query);

        return $response;
    } else {
        return $response->withStatus(401);
    }
});

/**
 * GET getItems
 * Summary: Find all items
 * Notes: Returns data of all items
 * Output-Formats: [application/xml, application/json]
 */
$app->GET('/item', function ($request, $response, $args) {
    require_once('db.php');

    $sql = "SELECT 
      item_id as item_id,  
      item as item, 
      category as category, 
      price as price
      FROM menu";

    /* @var $conn mysqli */
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }
    }
    /* @var $response \Slim\Http\Response */
    $response->write(json_encode($data));
});

/**
 * PUT updateitem
 * Summary: Update data of an existing item
 * Notes:
 * Output-Formats: [application/xml, application/json]
 */
$app->PUT('/item/{item_id}', function ($request, $response, $args) {
    require_once('db.php');
    require_once('checkLogin.php');

    if (checkToken()) {
        $get_id = $request->getAttribute('item_id');

        $query = "UPDATE menu SET item = ?, category = ?, price = ? WHERE item_id = $get_id";

        /* @var $conn */
        $stmt = $conn->prepare($query);

        $stmt->bind_param("ssi", $item, $category, $price);

        $item = $request->getParsedBody()['item'];

        $category = $request->getParsedBody()['category'];

        $price = $request->getParsedBody()['price'];


        $stmt->execute();

        $body = $request->getParsedBody();

        return $response;
    } else {
        return $response->withStatus(401);
    }
});

$app->POST('/login', function ($request, $response, $args) {

    $login_username = addslashes($request->getParsedBody()["username"]);
    $login_password = addslashes($request->getParsedBody()["password"]);

    require_once('db.php');

    $query = "SELECT * FROM users WHERE username='$login_username' && password='$login_password'";
    $result = $conn->query($query);

    $data = [];

    while ($row = $result->fetch_assoc()) {
        $data[] = $row;
    }

    if ($data) {
        function generateRandomString($length = 10)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        $query = "UPDATE users SET token_value = (?) WHERE username = '$login_username' && password = '$login_password'";

        $stmt = $conn->prepare($query);

        $token_value = generateRandomString();

        $stmt->bind_param("s", $token_value);
        $stmt->execute();

        setcookie("token", $token_value, time() + 36000, "/");
        return $response;
    } else {
        return $response->withStatus(401);
    }

});

$app->GET('/logout', function ($request, $response, $args) {
    require_once('db.php');

    $token = $_COOKIE["token"];

    $query = "UPDATE users SET token_value = (?) WHERE token_value = '$token'";

    /* @var $conn */
    $stmt = $conn->prepare($query);
    $null = null;
    $stmt->bind_param("s", $null);
    $stmt->execute();


    // Delete the session cookie to get a new session ID on the next page load
    setcookie(
        "token",
        '',
        time() - 42000,
        "/"
    );

    return $response;
});


$app->run();
