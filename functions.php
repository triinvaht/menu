<?php

function getValue($table, $prop, $value, $column) {
    global $db;
    $query = $db->prepare('SELECT * FROM `'.$table.'` WHERE `'.$prop.'` = :value');
    $query->execute(array('value' => $value));
    $row = $query->fetch(PDO::FETCH_ASSOC);
    return $row[$column];
}

?>