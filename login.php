<?php
include_once("inc.php");

if (isset($_POST['password']) && $_POST['password'] == 'admin' && (isset($_POST['username']) && $_POST['username'] == 'admin')) {
    $_SESSION['logged'] = true; // logib kasutaja sisse
    header('Location: index.html');
} elseif (isset($_POST['password']) && $_POST['password'] != 'admin' && (isset($_POST['username']) && $_POST['username'] != 'admin')) {
    echo '<div class="alert alert-danger" role="alert">
        Wrong username or password. Try again.</div>';
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Sisene artistide keskkonda</title>
    <!-- Bootstrap core CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Custom fonts for this template-->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Arsenal:400i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="css/login.css" rel="stylesheet">
</head>

<body class="bg-dark">

<div class="card card-login mt-5 align-self-center" style="width: 30%; margin-right: auto; margin-left: auto;">
    <div class="card-header">Logi sisse</div>
    <div class="card-body">
        <form class="needs-validation" novalidate action="" method="post">
            <div class="form-group">
                <label for="validation1">Kasutajanimi</label>
                <input type="text" id="validation1" class="form-control" placeholder="Kasutajanimi" name="username" required>
                <div class="invalid-feedback">
                    Try again!
                </div>
                <label for="validation2">Parool</label>
                <input type="password" id="validation2" class="form-control" placeholder="Parool" name="password" required>
                <div class="invalid-feedback">
                    Try again!
                </div>
            </div>
            <div class="form-group">
            </div>
            <button role="button" class="btn btn-outline-success btn-block" id="logisisse" name="login">Logi sisse</button>
        </form>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
</body>

</html>
