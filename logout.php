<?php
require_once('inc.php');

if(@$_SESSION['logged'] != 1) header("Location: index.html"); // kui kasutaja ei ole sisse logitud, siis suuname login lehele
$_SESSION['uid'] = -1; // määrame kasutaja uid -1 peale
$_SESSION['logged'] = 0; // määrame, et kasutaja on välja logitud
unset($_SESSION['uid']);
unset($_SESSION['logged']);

header("Location: login.php"); // suuname kasutaja login lehele
?>