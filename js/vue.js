new Vue({
    el: '#app',
    data: {
        loginModal: {
            username: null,
            password: null
        },
        loggedIn: false,
        allItems: [],
        item: {
            "iten_id": null,
            "item": null,
            "category": null,
            "price": null
        },
        addItem: {
            item: null,
            category: null,
            price: null
        },
        updateItem: {
            "item_id": null,
            "item": null,
            "category": null,
            "price": null
        },
        deleteItem: {
            "item_id": null
        },
        updatemodalItem: {
            "item_id": null,
            "item": null,
            "category": null,
            "price": null
        },
        openModal: {
            "item_id": null,
            "item": null,
            "category": null,
            "price": null
        },
    },
    mounted: function () {
        this.getAllItems();
        this.token = this.$cookies.get("token")
    },
    methods: {
        login() {
            this.$http.post('http://localhost/Menu/index.php/login', this.loginModal)
                .then(function (response) {
                    console.log(response);
                    this.loggedIn = true;
                });
        },

        logout() {
            this.$http.get('http://localhost/Menu/index.php/logout')
                .then(function (response) {
                    if (response.ok) {
                        this.loggedIn = false
                    }
                    console.log(response);
                });
        },

        getAllItems() {
            this.$http.get('http://localhost/Menu/index.php/item')
                .then(function (response) {
                    console.log(response);
                    this.allItems = response.body;
                });

        },

        insertItem() {
            console.log('test');
            this.$http.post('http://localhost/Menu/index.php/item', this.addItem)
                .then(function (response) {
                    console.log(response);
                    this.allItems.push(this.addItem);
                });

        },

        changeItem() {
            console.log('test');
            this.$http.put('http://localhost/Menu/index.php/item/' + this.updateItem.item_id, this.updateItem)
                .then(function (response) {
                    console.log(response);
                });
        },
        removeItem() {
            this.$http.delete('http://localhost/Menu/index.php/item/' + this.deleteItem.item_id)
                .then(function (response) {
                    console.log(response);
                });

        },
        changeModalItem() {
            console.log('test');
            this.$http.put('http://localhost/Menu/index.php/item/' + this.item.item_id, this.item)
                .then(function (response) {
                    console.log(response);
                });
        },
        open_modal: function (item_id) {
            var item = this.allItems.filter(function (currentItem) {
                return currentItem.item_id === item_id;
            })[0];
            this.item = item;
            console.log(item);
        },
        removeModalItem() {
            this.$http.delete('http://localhost/Menu/index.php/item/' + this.item.item_id)
                .then(function (response) {
                    console.log(response);
                    location.reload();
                });
        }
    }
});
