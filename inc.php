<?php

session_start();
ob_start();

include_once('config.php');
include_once('functions.php');

// conn
$db = new PDO('mysql:host=' . $dbParams['host'] . ';port=3306;dbname=' . $dbParams['dbname'] . ';charset=utf8',
    $dbParams['username'], $dbParams['password']);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

?>
